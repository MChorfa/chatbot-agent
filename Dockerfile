FROM rasa/rasa:2.2.6-full
COPY ./*.yml /app/
COPY ./data /app/data/
COPY ./tests /app/tests/
WORKDIR /app
ENTRYPOINT [""] 
